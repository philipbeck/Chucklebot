var Twit = require('twit');

//those random strings of characters are basically login information
var T = new Twit({
  //put correct values in here
 consumer_key: '' ,
 consumer_secret: '' ,
 access_token: '' ,
 access_token_secret: ''
});

//regexs
var ohDear = /oh dear($|\W$|\W\W)/i;

//TODO MAIN PROGRAM
loop();

function replyChuckle(){
  T.get('search/tweets',{q: '"oh dear"', result_type: "recent"},function(err, data, response){
    var replyId = data.statuses[0].id_str;
    var name = data.statuses[0].user.screen_name;

    console.log(data.statuses[0]);
    //console.log(data.statuses[0].text);
    console.log(name);
    //use a regex to make sure that the new two characters after "oh dear" are not letters
    if(name == "chuckle_bot"){//make sure the chucklebot doesn't end up in a loop
      console.log("not tweeted");
    }
    else if(ohDear.exec(data.statuses[0].text)){
      var tweet = "@" + name + " Oh dear oh dear...";
      console.log(tweet);
      T.post("statuses/update", {in_reply_to_status_id: replyId, status: tweet}, function(err, data, response) {
        console.log(err);
      });
      console.log("tweeted oh dear");
    }
    else{
      console.log("not tweeted");
    }
  });
}

function loop(){
  setTimeout(function(){
    replyChuckle();
    loop();
  }, 5000);
}
